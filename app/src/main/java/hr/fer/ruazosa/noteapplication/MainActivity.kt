package hr.fer.ruazosa.noteapplication

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.note_list_element.*


class MainActivity : AppCompatActivity() {

    lateinit var notesAdapter: NotesAdapter
    lateinit var viewModel: NotesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)  // ovo se uvijek zove
        setContentView(R.layout.activity_main) // postavlja activity_main na ekran

        listOfNotesView.layoutManager = LinearLayoutManager(applicationContext)  // dajemo recyclerview-u managera, moramo dinamički

        val decorator = DividerItemDecoration(applicationContext, LinearLayoutManager.VERTICAL)  //gluposti za recyclerview
        decorator.setDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.cell_divider)!!) // -||-
        listOfNotesView.addItemDecoration(decorator) // -||-

        viewModel =
            ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory(application)).get(
                NotesViewModel::class.java)  // kreiramo viewmodel i dajemo mu NotesViewModel, malo ga se mora specificno kreirat zato je tako ruzno

        notesAdapter = NotesAdapter(viewModel, {deletePressed(it)}, {onItemClicked(it)})   //dajemo viewModel u naš adapter za recyclerView
        listOfNotesView.adapter = notesAdapter   //postavljamo dinamički kreirani adapter, da je adapter kojeg smo definirali u XML-u

        viewModel.notesList.observe(this, Observer {
            notesAdapter.notifyDataSetChanged()
        })      //kad se izmjeni notesList u viewModelu, triggerat ce se notesAdapter.notifyblabla funkcija, a ona će se promijeniti samo kad se pokrene onResume

        newNoteActionButton.setOnClickListener {
            val intent = Intent(this, NotesDetails::class.java)
            startActivity(intent)
        }
    }

    private fun onItemClicked(position: Int) {
        val intent = Intent(this, NotesDetails::class.java)
        intent.putExtra("POSITION", position)
        startActivityForResult(intent, 19) // ne treba nam???
    }


    override fun onResume() {
        super.onResume()
        viewModel.getNotesRepository()
    }

    private fun deletePressed(note: Note){
        viewModel.deleteNote(note)
    }
}
